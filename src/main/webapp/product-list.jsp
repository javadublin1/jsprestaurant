<%@ page import="com.javadub1.jsprestaurant.model.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ page import="com.javadub1.jsprestaurant.model.Product" %>
<%@ page import="java.util.Set" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 8/17/19
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Product List</title>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Lista produktów</h1>
<%
    Long orderId = Long.parseLong(request.getParameter("orderId")); // mamy to w URL
%>
<a href="/product/add?orderId=<%= orderId %>">Dodaj produkt</a>
<%--<a href="/product/add?orderId=<% out.print(orderId); %>">Dodaj produkt</a>--%>

<table width="100%">
    <tr>
        <th>Id</th>
        <th>Nazwa produktu:</th>
        <th>Wartość:</th>
        <th>Ilość:</th>
        <th>Akcje:</th>
    </tr>
    <%
        Set<Product> productSet = (Set<Product>) request.getAttribute("productSetAttribute");
        for (Product product : productSet) {
            out.print("<tr>");
            out.print("<td>" + product.getId() + "</td>");
            out.print("<td>" + product.getDescription() + "</td>");
            out.print("<td>" + product.getValue() + "</td>");
            out.print("<td>" + product.getAmount() + "</td>");
            out.print("<td>" +
                    getLinkToRemoveProduct(orderId, product.getId()) +
                    getLinkToEditProduct(orderId, product.getId()) +
                    "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
<%!
    private String getLinkToEditProduct(Long orderId, Long id) {
        return "<a href=\"/product/edit?orderId=" + orderId + "&productId=" + id + "\">Edytuj</a>";
    }

    private String getLinkToRemoveProduct(Long orderId, Long id) {
        return "<a href=\"/product/remove?orderId=" + orderId + "&productId=" + id + "\">Usun</a>";
    }
%>