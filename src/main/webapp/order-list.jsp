<%@ page import="com.javadub1.jsprestaurant.model.Order" %>
<%@ page import="java.util.List" %>
<%@ page import="java.time.format.DateTimeFormatter" %><%--
  Created by IntelliJ IDEA.
  User: amen
  Date: 8/17/19
  Time: 1:57 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Order List</title>
</head>
<body>
<jsp:include page="navigator.jsp"/>
<h1>Lista zamówień</h1>
<table width="100%">
    <tr>
        <th>Id</th>
        <%--<th>Ilość produktów:</th>--%>
        <th>Czas złożenia zamówienia:</th>
        <th>Czas dostarczenia:</th>
        <th>Ilość osób:</th>
        <th>Numer stolika:</th>
        <th>Do zapłaty:</th>
        <th>Czy zapłacono:</th>
        <th>Akcje:</th>
    </tr>
    <%
        List<Order> orderList = (List<Order>) request.getAttribute("orderListAttribute");
        for (Order order : orderList) {
            out.print("<tr>");
            out.print("<td>" + order.getId() + "</td>");
//            out.print("<td>"+order.getProducts().size()+"</td>");
            out.print("<td>" + order.getFormattedTimeOrdered() + "</td>");
            out.print("<td>" + order.getFormattedTimeDelivered() + "</td>");
            out.print("<td>" + order.getPeopleCount() + "</td>");
            out.print("<td>" + order.getTableNumber() + "</td>");
            out.print("<td>" + String.format("%.2f", order.calculateToPay()) + "</td>");
            out.print("<td>" + (order.getPaid() != null) + "</td>");
            out.print("<td>" +
                    getLinkForProductList(order.getId()) +
                    getLinkForDeliveringOrder(order) +
                    getLinkForPaidOrder(order) +
                    getLinkToRemoveOrder(order) +
                    // tutaj - link "Zapłacono" - link powinien pojawić się tylko jeśli
                    // order jest dostarczony. Tworzymy nowy link i nowy serwlet.
                    "</td>");
            out.print("</tr>");
        }
    %>
</table>
</body>
</html>
<%!
    private String getLinkToRemoveOrder(Order order) {
        if (order.getProductsCount() == 0) {
            return "<a href=\"/order/remove?orderId=" + order.getId() + "\">Usun</a>";
        } else {
            return "";
        }
    }

    private String getLinkForPaidOrder(Order order) {
        if (order.calculateToPay() > 0) {
            return "<a href=\"/order/paid?orderId=" + order.getId() + "\">Zapłacono</a>";
        } else {
            return "";
        }
    }

    private String getLinkForProductList(Long id) {
//         <a href="/product/list?orderId=5"> Produkty </a>
        return "<a href=\"/product/list?orderId=" + id + "\">Produkty</a>";
    }

    private String getLinkForDeliveringOrder(Order order) {
        if (order.getTimeDelivered() == null) {
            return "<a href=\"/order/deliver?orderId=" + order.getId() + "\">Dostarczono</a>";
        } else {
            return "";
        }
    }
%>